#' Make an IS intensity bar plot
#'
#' @param data dataframe in format of x, y, colour.
#'
#' @return ggplot object containing the bar plot.
#' @export
#' @import ggplot2
#'
#' @examples
#' df <- data.frame(nuc=c(101:110),
#'                  int=c(1:10),
#'                  col=rep(c("1","2"),5))
#' plot_intensity(df)
plot_intensity <- function(data){
  # This function will make a bar plot intended for showing IS intensity peaks.
  #
  # Args:
  # data - dataframe in format of x, y, colour
  cols <- colnames(data)
  dat <- aes_string(
    as.name(cols[1]),
    as.name(cols[2]),
    group = as.name(cols[1]),
    fill = as.name(cols[3]))
  ggplot(data, dat) +
    geom_col(position = "dodge") +
    scale_x_continuous(breaks=seq(100, max(data[,1]), by=100))+
    scale_color_brewer(palette="Set1")
}
