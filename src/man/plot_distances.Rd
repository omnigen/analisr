% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/beta_diversity.R
\name{plot_distances}
\alias{plot_distances}
\title{Plot distance boxplot with anova}
\usage{
plot_distances(data, grouping = "group")
}
\arguments{
\item{data}{Dataframe containing variables \code{grouping}, distances and Phylum.}

\item{grouping}{Variable containing grouping values in \code{data}.}
}
\value{
ggplot object
}
\description{
Plot distance boxplot with anova
}
\details{
This function plots the Bray curtis distances facetted by Phylum.
}
\examples{
spreadall <- data.frame(
  Sample_id = rep(1:24, each = 4),
  group = rep(c("x","y"), each = 48),
  Phylum = c("AllPhyla", "a", "b", "c"),
  a = c(sample(1:8, 48, replace = T), sample(8:15, 48, replace = T)),
  b = c(sample(1:8, 48, replace = T), sample(8:15, 48, replace = T)),
  c = c(sample(1:8, 48, replace = T), sample(8:15, 48, replace = T)),
  d = c(sample(1:8, 48, replace = T), sample(1:15, 48, replace = T)),
  e = c(sample(1:8, 48, replace = T), sample(1:15, 48, replace = T)),
  f = c(sample(1:8, 48, replace = T), sample(1:15, 48, replace = T))
)

datalist <- by(spreadall, spreadall$Phylum, calculate_dissimilarity)

# merge the datasets in one without PCoA's
df <- Reduce(function(df1, df2) {
    dplyr::bind_rows(df1[,1:4], df2[,1:4])
  }, lapply(datalist, function(x) x[[1]]))

plot_distances(df)
}
\seealso{
Other beta diversity: \code{\link{add_phyla}},
  \code{\link{calculate_dissimilarity}},
  \code{\link{find_hull}},
  \code{\link{plot_dissimilarity_all}},
  \code{\link{plot_dissimilarity}},
  \code{\link{valid_PCoA_name}}
}
