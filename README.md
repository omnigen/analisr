# analisr
Package for analysing IS-pro data and generate plots.

## Installation
### Manual
- Download "analisr_x.x.x.tar.gz"
- In R use the command "install.packages('path/to/package.tar.gz')"
### Git install
- Use R command: "devtools::install_git('https://bitbucket.org/omnigen/analisr', subdir = 'src')"


Use as any other R package.